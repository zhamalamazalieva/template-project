import { applyMiddleware, combineReducers, compose, createStore} from "redux";
import thunk from "redux-thunk"
const rootReducer = combineReducers({

})

const middleWares = [thunk]

const devTools = process.env.NODE_ENV !== "production" && window._REDUX_DEVTOOLS_EXTENSION__ ? window._REDUX_DEVTOOLS_EXTENSION__ && window._REDUX_DEVTOOLS_EXTENSION__() : (a) => a;

const store = createStore(rootReducer, compose(applyMiddleware(...middleWares), devTools))

export default store