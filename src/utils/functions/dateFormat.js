import { getCurrentLanguage } from "./currentLanguage";

export const ListOfYears = () => {
	let currentYear = new Date().getFullYear();
	let years = [];
	for (let i = currentYear; i > 1980; i--) {
		years.push(i);
	}
	return years;
};

export function formatDate(date_str) {
	const selectedLanguage = getCurrentLanguage();
	const formatted_date = new Date(date_str);
	let options = {
		year: "numeric",
		month: "long",
		day: "numeric",
	};
	return formatted_date.toLocaleDateString(
		`${selectedLanguage === "en" ? "en-EN" : "ru-RU"}`,
		options
	);
}
