import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import ru from "./ru.json"
import en from "./en.json"
import { getCurrentLanguage } from "../utils/functions/currentLanguage";


const Languages = {
	en:{
		translation:en
	},
	ru:{
		translation:ru
	}
}
i18n
	.use(initReactI18next)
	.init({
		resources:Languages,
		fallbackLng:ru,
		interpolation:{
			escapeValue:false
		}
	})
localStorage.setItem("currentLanguage", getCurrentLanguage())

export default i18n