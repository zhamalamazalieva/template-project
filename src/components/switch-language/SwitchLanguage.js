import i18n from "../../i18next";


export const SwitchLanguage = () => {
	const  changeLang = (lang) => {
		i18n.changeLanguage(lang)
		localStorage.setItem("currentLanguage", lang)
	}
	return(
		<>
			<button onClick={() => changeLang("ru")}>change language ru</button>
			<button onClick={() => changeLang("en")}>change language en</button>
		</>
	)
}