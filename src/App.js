import { useTranslation } from "react-i18next";
import { SwitchLanguage } from "./components/switch-language/SwitchLanguage";
import { formatDate } from "./utils/functions/dateFormat";
import Toastify  from "./utils/helpers/toaster"
import { TOASTIFY_MESSAGES } from "./utils/constants/toastifyMessages";


function App() {
  const {t } = useTranslation()
  const onClick = () => {
    Toastify("success", TOASTIFY_MESSAGES.SUCCESSFULLY_CREATED)
  }

  return (
      <header className="App-header">
        <h1 onClick={() => onClick()}>{formatDate(new Date())}</h1>
        <SwitchLanguage/>
        {t("hello")}
      </header>
  );
}

export default App;
