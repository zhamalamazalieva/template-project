import React from "react";
import { Provider } from "react-redux";
import store from "./store/store";
import i18n from "./i18next";
import { I18nextProvider } from "react-i18next"
import { ToastContainer } from "react-toastify";

export default function Providers({ children }) {
	return (
		<I18nextProvider i18n={i18n}>
			<ToastContainer />
				<Provider store={store}>{children}</Provider>
		</I18nextProvider>
	);
}
